/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package comp101conversion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Ben
 */
public class fileComp implements Runnable{
int rank;
    @Override
    public void run()
    {
        Global.hm0.clear();
        Global.hm1.clear();
        Integer temp = null;
        int n = Global.StringFileContent.length;
        int begin =  (n/Global.threadCount)*rank;
        int end = begin + (n/Global.threadCount)-1;
        if(rank == Global.threadCount - 1)
		end = n - 1;
        // String frequency of as0 split here for parallelism      
        for (int i = begin; i < end; i++) {
            temp = Global.hm0.get(Global.StringFileContent[i]);
            if (temp == null) {
                Global.hm0.put(Global.StringFileContent[i], new Integer(1));
            } else {
                Global.hm0.put(Global.StringFileContent[i], new Integer(temp.intValue() + 1));
            }
        }

        // String frequency of as1 //split for parallelism
        n = Global.StringFileContent1.length;
        begin =  (n/Global.threadCount)*rank;
        end = begin + (n/Global.threadCount)-1;
        if(rank == Global.threadCount - 1)
		end = n - 1;
        for (int i = begin; i < end; i++) {
            temp = Global.hm1.get(Global.StringFileContent1[i]);
            if (temp == null) {
                Global.hm1.put(Global.StringFileContent1[i], new Integer(1));
            } else {
                Global.hm1.put(Global.StringFileContent1[i], new Integer(temp.intValue() + 1));
            }
        }
    }
    public void rankSet(int r){rank = r;}
    
    public static void input1()
    {
         String s1="";
         Scanner sc = null;
                try
                {
                    sc = new Scanner(new File(Global.fileCompFName1));
                }
                catch(FileNotFoundException e)
                {
                     System.out.println("The source file does not exist. " + e);
                }
                
                s1 = sc.nextLine();
                //char[] charArray = new char [100000];
                while (sc.hasNextLine()) {
                     s1 = s1 + "\n" + sc.nextLine();
                }
                
                s1 = s1.trim().replaceAll("[.?!]", " ");
                 //Split by space
                Global.StringFileContent = s1.split(" ");
    }
    
    public static void input2()
    {
        String s2="";
        Scanner sc = null;
        try
                {
                    sc = new Scanner(new File(Global.fileCompFName2));
                }
                catch(FileNotFoundException e)
                {
                     System.out.println("The source file does not exist. " + e);
                }
                
                s2 = sc.nextLine();
                //char[] charArray = new char [100000];
                while (sc.hasNextLine()) {
                     s2 = s2 + "\n" + sc.nextLine();
                }
                
                s2 = s2.trim().replaceAll("[.?!]", " ");
                 //Split by space
                Global.StringFileContent1 = s2.split(" ");
    }
    public static void percentageOfMatch() {
        // Frequency difference between hm0 and hm1 to diff
     HashMap<String, Integer> diff = new HashMap<>();
        String key;
        Integer value, value1, rval;
        Iterator it = Global.hm0.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it.next();
            key = pairs.getKey();
            value = pairs.getValue();
            value1 = Global.hm1.get(key);
            it.remove();
            Global.hm1.remove(key);
            if (value1 != null)
                rval = new Integer(Math.abs(value1.intValue()- value.intValue()));
            else
                rval = value;
            diff.put(key, rval);
        }

        // Sum all remaining String frequencies in hm1
        int val = 0;
        it = Global.hm1.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it.next();
            val += pairs.getValue().intValue();
        }
        
        // Sum all frequencies in diff
        it = diff.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it.next();
            val += pairs.getValue().intValue();
        }

        // Calculate word match percentage
        int per = (int) ((((float) val * 100)) / ((float) (Global.StringFileContent.length + Global.StringFileContent1.length)));
        per = 100 - per;
        Global.percentage = per;
    }
}

