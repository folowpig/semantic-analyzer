/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package comp101conversion;

/**
 *
 * @author Ben
 */
public class WordCounter implements Runnable{
    int count;
    int rank;
    int contentLength;
    boolean phraseBool;
    boolean totPhrase;
    public void contentLengthSet(int c){contentLength = c;}
    public void rankSet(int r){rank = r;}
    public int getCount(){return count;}
    public void setPhraseBool(boolean b){phraseBool = b;}
    @Override
    public void run() {thrInstanceCnt(rank);}
   

//This is a function that returns the next space separated token
String nxtStrTok(char[] str, int ind)
{
	String token = "";
	while(str[ind] == ' ')
	{	ind++;}
	while((str[ind] != ' ')&&(str[ind] != '\0'))
	{
		token += str[ind];
		ind++;
	}
	return token;
}
String nxtStrPhrase (char[] str, int ind)
{
    int localCount= 0;
    String token = "";
	while(str[ind] == ' ')
	{	ind++;}
        if (str[ind] == (Global.wordToSearch.toCharArray()[0]))
        {
	while((str[ind] != '\0')&&localCount!=Global.wordToSearch.length())
	{       localCount++;
		token += str[ind];
		ind++;
	}
        }
        //System.out.println("PARSED TOKEN: " + token + ", ");
	return token;
}

    public void thrInstanceCnt(int rank)
{
    //count = 0;
    //System.out.println("TOTAL = " + contentLength);
    //Finding the beginning and end indices based on rank
	int begin =  (contentLength/Global.threadCount)*rank;
	int end = begin + (contentLength/Global.threadCount)-1;
	if(rank == Global.threadCount - 1)
		end = contentLength - 1;

	//Make the beginning index the next blank space
	if(rank != 0)
	{
		begin--;
		while(Global.charFileContent[begin] != ' ')
		{
			begin++;
		}
	}

	//Make the ending index the next blank space
        //System.out.print("END IS = " + end + " ");
	while(Global.charFileContent[end] != ' '&& end != contentLength-1)
	{
		end++;
	}

	//try {
        //mutex.acquire();
        //try {
        //     System.out.println(rank + ": " + begin + ", "+end);
        //} finally {
        //mutex.release();
        //}
       // } catch(InterruptedException ie) {}


	//Count the instances of the word to search
	String curWord;
	while((end - begin) > 1)
	{
                if (phraseBool == false)
                {curWord = nxtStrTok(Global.charFileContent, begin);begin += curWord.length() + 1;}
                else
                {curWord =nxtStrPhrase(Global.charFileContent, begin);begin += curWord.length() + 1;}
                
		if(curWord.equals(Global.wordToSearch))
		{
			count++;
		}
	}

	//Adding this threads value to the total count
	
}

}
