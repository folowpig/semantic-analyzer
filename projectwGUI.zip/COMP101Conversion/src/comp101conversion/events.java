/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package comp101conversion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Ben
 */
public class events {
    
    //WordDuplication functions
    public static void inputFile() //load in file with path obtained from browse button
    {
        String theString;
        File file = new File(Global.filename); //REPLACE WITH BROWSE DATA
    try
    {
      Scanner sc = new Scanner(file);
      theString = sc.nextLine();
      while (sc.hasNextLine()) {theString = theString + "\n" + sc.nextLine();}
      Global.charFileContent= theString.toCharArray();
      Global.fileind= theString.length();
    }
    
    catch(FileNotFoundException e)
    {
      System.out.println("The source file does not exist. " + e);
    }    
	Global.charFileContent[Global.fileind-1] = '\0'; //ADD NULL TERMINUS TO END OF FILE
//REMOVE ALL PUNCTUATION AND REPLACE WITH A SPACE AND MAKE ALL CAPITAL LETTERS LOWER CASE
	char c;
	for(int x=0; x < Global.fileind; x++)
	{
		c = Global.charFileContent[x];
		if((c=='.')||(c==',')||(c==':')||(c==';')||(c=='-')||(c=='/')||(c=='!')||(c=='?')||(c=='\n'))
		{
			Global.charFileContent[x] = ' ';
		}
		else
		{
			Global.charFileContent[x] = Character.toLowerCase(Global.charFileContent[x]);
		}
	}
       // COMP101Conversion.ProgressChecker.setText(Global.minifilename);*****************
    }    
    public static void userInput() //spawns threads of type wordDuplication
    {
        //Getting the word to be searched for
        
	Global.wordToSearch = COMP101Conversion.dupInput.getText();//input.nextLine();*******************
        
        boolean phraseCheck = false;
        char [] temp = Global.wordToSearch.toCharArray();
        for(int i=0;i<Global.wordToSearch.length();i++) //Checks if the search query is a phrase
        {
            if (temp[i]==(' '))
            {phraseCheck = true; break;}
        }
        
	//Creating threads and starting them with their rank
	Thread [] threads = new Thread[Global.threadCount];
        WordCounter [] results = new WordCounter[Global.threadCount];
	for(int thr=0; thr < Global.threadCount; thr++)
	{
             results[thr] = new WordCounter();
             results[thr].setPhraseBool(phraseCheck);
             results[thr].contentLengthSet(Global.fileind);
             results[thr].rankSet(thr);
             threads[thr] = new Thread(results[thr]);
             threads[thr].start();
	}

	for (int i = 0; i < Global.threadCount; i++)
        {
            try
            {
                threads[i].join();
            } 
            catch (InterruptedException e)
            {
                System.out.println("Exception Error");
            }
        }
        for (int i = 0; i <Global.threadCount;i++)
        {
            
            Global.wordCount+=results[i].getCount();
            //System.out.println(Global.wordCount);
        }
    }
    
    //FileComparison functions
    public static void fileCompI1()
    {
        fileComp.input1();
    }
    public static void fileCompI2()
    {
        fileComp.input2();
    }
    public static void fileCompThreads()
    {
        Thread [] threads = new Thread[Global.threadCount];
        fileComp [] results = new fileComp[Global.threadCount];
	for(int thr=0; thr < Global.threadCount; thr++)
	{
             results[thr] = new fileComp();
             results[thr].rankSet(thr);
             threads[thr] = new Thread(results[thr]);
             threads[thr].start();
	}
        for (int i = 0; i < Global.threadCount; i++)
        {
            try
            {
                threads[i].join();
            } 
            catch (InterruptedException e)
            {
                System.out.println("Exception Error");
            }
        }
       fileComp.percentageOfMatch();
        
    }
    
    public static void wordAssoInput()
    {
        String theString;
        File file = new File(Global.fileWordAsso); //REPLACE WITH BROWSE DATA
    try
    {
      Scanner sc = new Scanner(file);
      theString = sc.nextLine();
      while (sc.hasNextLine()) {theString = theString + "\n" + sc.nextLine();}
      Global.charFileContent2= theString.toCharArray();
      Global.fileind2= theString.length();
    }
    
    catch(FileNotFoundException e)
    {
      System.out.println("The source file does not exist. " + e);
    }    
	Global.charFileContent2[Global.fileind2-1] = '\0'; //ADD NULL TERMINUS TO END OF FILE
//REMOVE ALL PUNCTUATION AND REPLACE WITH A SPACE AND MAKE ALL CAPITAL LETTERS LOWER CASE
	char c;
	for(int x=0; x < Global.fileind; x++)
	{
		c = Global.charFileContent2[x];
		if((c=='.')||(c==',')||(c==':')||(c==';')||(c=='-')||(c=='/')||(c=='!')||(c=='?')||(c=='\n'))
		{
			Global.charFileContent2[x] = ' ';
		}
		else
		{
			Global.charFileContent2[x] = Character.toLowerCase(Global.charFileContent[x]);
		}
	}
    }
}
    
    

