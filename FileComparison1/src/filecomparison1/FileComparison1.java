/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package filecomparison1;

import java.io.*;
import java.util.*;
/**
 *
 * @author Edward
 */
public class FileComparison1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)throws java.io.IOException
    {
		//Getting the name of the files to be compared.
		BufferedReader br2 = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter 1st File name:");
  		String str = br2.readLine();
  		System.out.println("Enter 2nd File name:");
  		String str1 = br2.readLine();

		String s1="";
  		String s2="";

        //Reading the contents of the files
                Scanner sc = null;
                try
                {
                    sc = new Scanner(new File(str));
                }
                catch(FileNotFoundException e)
                {
                     System.out.println("The source file does not exist. " + e);
                }
                
                s1 = sc.nextLine();
                //char[] charArray = new char [100000];
                while (sc.hasNextLine()) {
                     s1 = s1 + "\n" + sc.nextLine();
                }
 		
		try
                {
                    sc = new Scanner(new File(str1));
                }
                catch(FileNotFoundException e)
                {
                     System.out.println("The source file does not exist. " + e);
                }
                
                s2 = sc.nextLine();
                //char[] charArray = new char [100000];
                while (sc.hasNextLine()) {
                     s2 = s2 + "\n" + sc.nextLine();
                }
                
                s1 = s1.trim().replaceAll("[.?!]", " ");
                s2 = s2.trim().replaceAll("[.?!]", " ");
                 //Split by space
                String[] as1 = s1.split(" ");
                String[] as2 = s2.split(" ");
          System.out.println("Percent Similar : " + pecentageOfMatch(as1, as2) + "%");
        
    }
    /*public static int LevenshteinDistance(String s0, String s1) {

        int len0 = s0.length() + 1;
        int len1 = s1.length() + 1;

        // the array of distances
        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        // initial cost of skipping prefix in String s0
        for (int i = 0; i < len0; i++)
            cost[i] = i;

        // dynamicaly computing the array of distances

        // transformation cost for each letter in s1
        for (int j = 1; j < len1; j++) {

            // initial cost of skipping prefix in String s1
            newcost[0] = j - 1;

            // transformation cost for each letter in s0
            for (int i = 1; i < len0; i++) {

                // matching current letters in both strings
                int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int cost_replace = cost[i - 1] + match;
                int cost_insert = cost[i] + 1;
                int cost_delete = newcost[i - 1] + 1;

                // keep minimum cost
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete),
                        cost_replace);
            }

            // swap cost/newcost arrays
            int[] swap = cost;
            cost = newcost;
            newcost = swap;
        }

        // the distance is the cost for transforming all letters in both strings
        return cost[len0 - 1];
    }
   */ 
    public static int pecentageOfMatch(String[] as0, String[] as1) {
        int n = as0.length;
        Integer temp = null;
        
        // String frequency of as0 
        HashMap<String, Integer> hm0 = new HashMap<String, Integer>();
        for (int i = 0; i < n; i++) {
            temp = hm0.get(as0[i]);
            if (temp == null) {
                hm0.put(as0[i], new Integer(1));
            } else {
                hm0.put(as0[i], new Integer(temp.intValue() + 1));
            }
        }

        // String frequency of as1
        n = as1.length;
        HashMap<String, Integer> hm1 = new HashMap<String, Integer>();
        for (int i = 0; i < n; i++) {
            temp = hm1.get(as1[i]);
            if (temp == null) {
                hm1.put(as1[i], new Integer(1));
            } else {
                hm1.put(as1[i], new Integer(temp.intValue() + 1));
            }
        }

        // Frequency difference between hm0 and hm1 to diff
        HashMap<String, Integer> diff = new HashMap<String, Integer>();
        String key;
        Integer value, value1, rval;
        Iterator it = hm0.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
                    .next();
            key = pairs.getKey();
            value = pairs.getValue();
            value1 = hm1.get(key);
            it.remove();
            hm1.remove(key);
            if (value1 != null)
                rval = new Integer(Math.abs(value1.intValue()
                        - value.intValue()));
            else
                rval = value;
            diff.put(key, rval);
        }

        // Sum all remaining String frequencies in hm1
        int val = 0;
        it = hm1.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
                    .next();
            val += pairs.getValue().intValue();
        }
        
        // Sum all frequencies in diff
        it = diff.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
                    .next();
            val += pairs.getValue().intValue();
        }

        // Calculate word match percentage
        int per = (int) ((((float) val * 100)) / ((float) (as0.length + as1.length)));
        per = 100 - per;
        return per;
    }
}
