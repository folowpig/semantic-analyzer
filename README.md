# Welcome to Semantic Analyzer, an application that analyze and compare text documents! #


**Team:**  ACE

**Members:** Ben Chou, Hassan Jahami, Edward Kim

**The project was developed using Waterfall development method.**

**Individual Contributions:**
I managed project related documents such as requirement analysis, systems design, and weekly reports. Also created the version control to manage project files.
I was responsible for development file comparison module which gives out percentage of similarity between two different text files.

**Team Goals:**
- Produce an application which analyzes text files in parallel yielding meaningful data.
- Inspired by minimalist design philosophy.
- Modular design for easy extension and maintenance.
- Optimize algorithm for quick access.
- Analysis is time consuming; automation could greatly increase productivity if accurate.

**Overview**

![Sementic.JPG](https://bitbucket.org/repo/4MkaBg/images/3525043631-Sementic.JPG)

This is a layout of feature Word Duplication that finds number of particular word or phrase.

![Capture.JPG](https://bitbucket.org/repo/4MkaBg/images/1177420240-Capture.JPG)

This is a File Comparison feature that compares two text documents and outputs similarity in percentage.

![Capture1.JPG](https://bitbucket.org/repo/4MkaBg/images/669167845-Capture1.JPG)

This is a Word Association feature that outputs top 10 most used words with its number of usage.