/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package comp101conversion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Ben
 */
public class events {
    
    public static void inputFile(String s)
    {
        String theString;
        File file = new File(s); //REPLACE WITH BROWSE DATA
    try
    {
      Scanner sc = new Scanner(file);
      theString = sc.nextLine();
      while (sc.hasNextLine()) {theString = theString + "\n" + sc.nextLine();}
      Global.charFileContent= theString.toCharArray();
      Global.fileind= theString.length();
    }
    
    catch(FileNotFoundException e)
    {
      System.out.println("The source file does not exist. " + e);
    }    
	Global.charFileContent[Global.fileind-1] = '\0'; //ADD NULL TERMINUS TO END OF FILE
//REMOVE ALL PUNCTUATION AND REPLACE WITH A SPACE AND MAKE ALL CAPITAL LETTERS LOWER CASE
	char c;
	for(int x=0; x < Global.fileind; x++)
	{
		c = Global.charFileContent[x];
		if((c=='.')||(c==',')||(c==':')||(c==';')||(c=='-')||(c=='/')||(c=='!')||(c=='?')||(c=='\n'))
		{
			Global.charFileContent[x] = ' ';
		}
		else
		{
			Global.charFileContent[x] = Character.toLowerCase(Global.charFileContent[x]);
		}
	}
        COMP101Conversion.ProgressChecker.setText("Loading Complete");
        
    }
    
    public static void userInput()
    {
        //Getting the word to be searched for
	Global.wordToSearch = COMP101Conversion.WordConfirmBox.getText();//input.nextLine();
        
        boolean phraseCheck = false;
        char [] temp = Global.wordToSearch.toCharArray();
        for(int i=0;i<Global.wordToSearch.length();i++) //Checks if the search query is a phrase
        {
            if (temp[i]==(' '))
            {phraseCheck = true; break;}
        }
        
	//Creating threads and starting them with their rank
	Thread [] threads = new Thread[Global.threadCount];
        WordCounter [] results = new WordCounter[Global.threadCount];
	for(int thr=0; thr < Global.threadCount; thr++)
	{
             results[thr] = new WordCounter();
             results[thr].setPhraseBool(phraseCheck);
             results[thr].contentLengthSet(Global.fileind);
             results[thr].rankSet(thr);
             threads[thr] = new Thread(results[thr]);
             threads[thr].start();
	}

	for (int i = 0; i < Global.threadCount; i++)
        {
            try
            {
                threads[i].join();
            } 
            catch (InterruptedException e)
            {
                System.out.println("Exception Error");
            }
        }
        for (int i = 0; i <Global.threadCount;i++)
        {
            Global.wordCount+=results[i].getCount();
        }
    }
}
    
    

